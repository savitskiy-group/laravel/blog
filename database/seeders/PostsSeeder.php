<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++)
            DB::table('posts')->insert([
                'category_id' => rand(1, 9),
                'title' => 'Post ' . $i,
                'slug' =>'post-' . $i,
                'description' => 'Description of Post ' . $i,
                'text' => '<p>Post ' . $i . ' text: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad cum dignissimos fugiat mollitia nam nobis sapiente similique sint velit voluptate! Delectus in magni minus officia sed vero. Cum, dolores illo, magnam maiores nam obcaecati optio praesentium quam, quia quibusdam quis voluptatibus. Consequuntur eum inventore ipsam officia possimus, recusandae ut voluptates?<br>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ad cum dignissimos fugiat mollitia nam nobis sapiente similique sint velit voluptate! Delectus in magni minus officia sed vero. Cum, dolores illo, magnam maiores nam obcaecati optio praesentium quam, quia quibusdam quis voluptatibus. Consequuntur eum inventore ipsam officia possimus, recusandae ut voluptates!',
            ]);
    }
}
