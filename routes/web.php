<?php

use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\BlogController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [BlogController::class, 'index']);
Route::get('/category/{slug}', [BlogController::class, 'getPostsByCategory'])->name('getPostsByCategory');
Route::get('/category/{slug_category}/{slug_post}', [BlogController::class, 'getPost'])->name('getPost');

/*
22-09-19 Еще одно видео “Изучение Laravel в одном видео / Создание сайта на PHP Laravel за час!”, автора Гоша Дударь:
https://www.youtube.com/watch?v=0Be0fX9wbXc
*/

//Route::get('/home', 'MainController@home'); // Old method, don't work anymore
Route::get('/home', '\App\Http\Controllers\MainController@home'); // Old method, don't work

Route::get('/about', '\App\Http\Controllers\MainController@about');

Route::get('/reviews', '\App\Http\Controllers\MainController@reviews')->name('reviews');

Route::post('/reviews/check', '\App\Http\Controllers\MainController@reviews_check');


/*Route::get('/user/{id}/{name}', function($i5d, $n5ame) {
    return 'ID: ' . $i5d . ' Name: ' . $n5ame;
});*/